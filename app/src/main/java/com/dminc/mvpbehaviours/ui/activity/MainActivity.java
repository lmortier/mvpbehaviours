package com.dminc.mvpbehaviours.ui.activity;

import com.dminc.mvpbehaviours.R;
import com.dminc.mvpbehaviours.books.BooksFragment;
import com.dminc.mvpbehaviours.framework.Presenter;
import com.dminc.mvpbehaviours.framework.PresenterFactory;
import com.dminc.mvpbehaviours.framework.PresenterHolder;
import com.dminc.mvpbehaviours.framework.PresenterHolderFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements PresenterHolder {

    private PresenterHolder mPresenterHolder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generic);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(new PresenterHolderFragment(), "PRESENTER_HOLDER_FRAGMENT")
                    .add(R.id.fragment_container, new BooksFragment())
                    .commit();
        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof PresenterHolder) {
            mPresenterHolder = (PresenterHolder) fragment;
        }
    }

    @Override
    public <T extends Presenter<?>> T getOrCreatePresenter(PresenterFactory<T> presenterFactory) {
        return mPresenterHolder.getOrCreatePresenter(presenterFactory);
    }

    @Override
    public void destroyPresenter(PresenterFactory<?> presenterFactory) {
        mPresenterHolder.destroyPresenter(presenterFactory);
    }
}
