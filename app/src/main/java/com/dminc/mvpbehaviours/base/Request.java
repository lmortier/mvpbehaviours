package com.dminc.mvpbehaviours.base;

public interface Request {

    Request EMPTY = new RequestImpl();

    class RequestImpl implements Request {}
}
