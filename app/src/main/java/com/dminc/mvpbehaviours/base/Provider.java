package com.dminc.mvpbehaviours.base;

import java.util.List;

import rx.Observable;

public interface Provider<T> {

    Observable<T> getItem(long id);
    Observable<List<T>> getItems(Request request);

}
