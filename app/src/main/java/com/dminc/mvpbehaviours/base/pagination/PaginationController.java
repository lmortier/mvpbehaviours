package com.dminc.mvpbehaviours.base.pagination;

import android.support.annotation.Nullable;

import com.dminc.mvpbehaviours.base.Request;

/**
 * @author Lewie Mortier
 */

public interface PaginationController {

    @Nullable
    Request apply(Request request);

    void reset();

    void onRequestFinished();
}
