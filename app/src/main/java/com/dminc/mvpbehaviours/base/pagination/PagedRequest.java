package com.dminc.mvpbehaviours.base.pagination;

import com.dminc.mvpbehaviours.base.Request;

/**
 * @author Konrad
 */

public class PagedRequest implements Request {

    private Integer mOffset;
    private Integer mPageSize;
    public PagedRequest() {}

    public PagedRequest(Request request) {
        if (request instanceof PagedRequest) {
            mOffset = ((PagedRequest) request).getOffset();
            mPageSize = ((PagedRequest) request).getPageSize();
        }
    }

    public void setOffset(int offset) {
        mOffset = offset;
    }

    public void setPageSize(int pageSize) {
        mPageSize = pageSize;
    }

    public Integer getPageSize() {
        return mPageSize;
    }

    public Integer getOffset() {
        return mOffset;
    }

}
