package com.dminc.mvpbehaviours.base;

import com.dminc.mvpbehaviours.BuildConfig;
import com.dminc.mvpbehaviours.SlowDownInterceptor;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitProvider {

    private static Retrofit sRetrofit;

    public static Retrofit provideRetrofit() {
        if (sRetrofit == null) {
            sRetrofit = createRetrofit();
        }
        return sRetrofit;
    }

    private static Retrofit createRetrofit() {
        HttpLoggingInterceptor logs = new HttpLoggingInterceptor();
        logs.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient().newBuilder().addInterceptor(logs)
                .addInterceptor(new SlowDownInterceptor()).build();
        return new Retrofit.Builder()
                .client(client)
                .baseUrl(BuildConfig.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }
}
