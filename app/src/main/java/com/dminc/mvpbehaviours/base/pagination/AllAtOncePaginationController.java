package com.dminc.mvpbehaviours.base.pagination;

import com.dminc.mvpbehaviours.base.Request;

/**
 * A PaginationController that does not apply pagination
 */
public class AllAtOncePaginationController implements PaginationController {

    private boolean mFinished;

    @Override
    public Request apply(Request request) {
        if (mFinished) {
            return Request.EMPTY;
        } else {
            return request;
        }
    }

    @Override
    public void reset() {
        mFinished = false;
    }

    @Override
    public void onRequestFinished() {
        mFinished = true;
    }
}
