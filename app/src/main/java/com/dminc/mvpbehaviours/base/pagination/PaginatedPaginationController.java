package com.dminc.mvpbehaviours.base.pagination;

import com.dminc.mvpbehaviours.base.Request;

/**
 * A PaginationController that applies pagination of a given page size
 */
public class PaginatedPaginationController implements PaginationController {

    private int mPageSize;

    private int mCurrentPage;

    public PaginatedPaginationController(int pageSize) {
        mPageSize = pageSize;
    }

    @Override
    public Request apply(Request request) {
        PagedRequest pagedRequest = new PagedRequest(request);
        pagedRequest.setOffset(mCurrentPage * mPageSize);
        pagedRequest.setPageSize(mPageSize);

        return pagedRequest;
    }

    @Override
    public void reset() {
        mCurrentPage = 0;
    }

    @Override
    public void onRequestFinished() {
        mCurrentPage++;
    }
}
