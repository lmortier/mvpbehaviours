package com.dminc.mvpbehaviours;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public class SlowDownInterceptor implements Interceptor {

    public static final int DELAY_MILLIS = 10_000;

    @Override
    public Response intercept(Chain chain) throws IOException {
        try {
            Thread.sleep(DELAY_MILLIS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return chain.proceed(chain.request());
    }
}
