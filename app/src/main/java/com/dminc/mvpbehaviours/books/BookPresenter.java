package com.dminc.mvpbehaviours.books;

import java.util.List;

import com.dminc.mvpbehaviours.base.Provider;
import com.dminc.mvpbehaviours.base.Request;
import com.dminc.mvpbehaviours.base.pagination.PaginationController;
import com.dminc.mvpbehaviours.framework.Presenter;

import hugo.weaving.DebugLog;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BookPresenter extends Presenter<BooksVista> {

    private PaginationController mPaginationController;
    private Provider<Book> mProvider;
    private boolean mLoadingInProgress;

    public BookPresenter(PaginationController paginationController, Provider<Book> provider) {
        mPaginationController = paginationController;
        mProvider = provider;
    }

    @Override
    public void onStart() {
        loadNextPage();
    }

    public void refresh() {
        getVista().reset();
        mPaginationController.reset();
        loadNextPage();
    }

    @DebugLog
    public void loadNextPage() {
        getVista().showLoading(true);
        Request request = new BookRequest();
        request = mPaginationController.apply(request);
        if (mLoadingInProgress) {
            return;
        }
        mLoadingInProgress = true;
        mProvider.getItems(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Book>>() {
                    @Override
                    public void onCompleted() {
                        getVista().showLoading(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getVista().showLoading(false);
                        getVista().showError();
                        e.printStackTrace();
                    }

                    @DebugLog
                    @Override
                    public void onNext(List<Book> books) {
                        mPaginationController.onRequestFinished();
                        getVista().appendBooks(books);
                        mLoadingInProgress = false;
                    }
                });
    }
}
