package com.dminc.mvpbehaviours.books;

import java.util.List;

import com.dminc.mvpbehaviours.base.Provider;
import com.dminc.mvpbehaviours.base.Request;
import com.dminc.mvpbehaviours.base.RetrofitProvider;

import retrofit2.Retrofit;
import rx.Observable;

public class BookProvider implements Provider<Book> {

    private BookService mBookService;

    public BookProvider() {
        Retrofit retrofit = RetrofitProvider.provideRetrofit();
        mBookService = retrofit.create(BookService.class);
    }

    @Override
    public Observable<Book> getItem(long id) {
        return null;
    }

    @Override
    public Observable<List<Book>> getItems(Request request) {
        if (request == Request.EMPTY) {
            return Observable.empty();
        }
        BookRequest bookRequest = new BookRequest(request);
        return mBookService.getBooks(bookRequest.getOffset(), bookRequest.getPageSize());
    }
}
