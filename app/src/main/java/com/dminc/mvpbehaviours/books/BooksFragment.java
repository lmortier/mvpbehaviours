package com.dminc.mvpbehaviours.books;

import com.dminc.mvpbehaviours.R;
import com.dminc.mvpbehaviours.base.pagination.PaginatedPaginationController;
import com.dminc.mvpbehaviours.framework.PresenterFactory;
import com.dminc.mvpbehaviours.framework.PresenterHolder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class BooksFragment extends Fragment implements BooksVista, PresenterFactory<BookPresenter> {

    private static final int PAGE_SIZE = 10;

    private BookPresenter mPresenter;

    protected LinearLayoutManager mLayoutManager;

    private SwipeRefreshLayout mRefreshLayout;

    private BookListAdapter mAdapter;

    private RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = mLayoutManager.getChildCount();
            int totalItemCount = mLayoutManager.getItemCount();
            int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                    && firstVisibleItemPosition >= 0
                    && totalItemCount >= PAGE_SIZE) {
                mPresenter.loadNextPage();
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mPresenter = ((PresenterHolder) getActivity()).getOrCreatePresenter(this);
    }

    @Override
    public BookPresenter createPresenter() {
        return new BookPresenter(new PaginatedPaginationController(PAGE_SIZE),
                //new AllAtOncePaginationController()
                new BookProvider());
    }

    @Override
    public String getPresenterTag() {
        return BookPresenter.class.getSimpleName();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_paginated_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.full_list);
        mRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.refresh();
            }
        });

        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addOnScrollListener(mScrollListener);
        mAdapter = new BookListAdapter();
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.start(this);
    }

    @Override
    public void showLoading(boolean loading) {
        mRefreshLayout.setRefreshing(loading);
    }

    @Override
    public void showError() {
        Toast.makeText(getActivity(), R.string.error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void appendBooks(List<Book> books) {
        mAdapter.addBooks(books);
    }

    @Override
    public void reset() {
        mAdapter.setBooks(new ArrayList<Book>());
    }

}
