package com.dminc.mvpbehaviours.books;

import com.dminc.mvpbehaviours.framework.Vista;

import java.util.List;

public interface BooksVista extends Vista {

    void showLoading(boolean loading);

    void showError();

    void appendBooks(List<Book> books);

    void reset();
}
