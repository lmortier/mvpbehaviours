package com.dminc.mvpbehaviours.books;

import com.dminc.mvpbehaviours.base.Request;
import com.dminc.mvpbehaviours.base.pagination.PagedRequest;

public class BookRequest extends PagedRequest {

    public BookRequest() {
        super();
    }

    public BookRequest(Request request) {
        super(request);
    }
}
