package com.dminc.mvpbehaviours.books;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface BookService {

    @GET("api/v1/items")
    Observable<List<Book>> getBooks(
            @Query("offset") Integer offset,
            @Query("count") Integer count
    );
}
